package com.example.challenge4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challenge4.databinding.FragmentLoginPageBinding

class Login_Page : Fragment() {
    private var _binding: FragmentLoginPageBinding? = null
    private val binding get() = _binding!!
    private val sharedPreferences = "Illyasviel Kawaii"
    var a = 0
    var b = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val sharedPreferences: SharedPreferences =
            requireActivity().getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        val namauser = sharedPreferences.getString("nama", "")
        val surel = sharedPreferences.getString("surel", "")
        val sandi = sharedPreferences.getString("sandi", "")
        val c = sharedPreferences.getInt("Skip",0)
        _binding = FragmentLoginPageBinding.inflate(inflater, container, false)
        if (c==1){
            skip()
        }
        binding.regis.setOnClickListener {
            it.findNavController().navigate(R.id.action_login_Page_to_register)
            val callback = object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    b++
                    if (b==2){System.exit(0)
                    }
                }
            }
            requireActivity().onBackPressedDispatcher.addCallback(callback)
        }
        binding.masuk.setOnClickListener {

            if (surel.equals(binding.email.text.toString()) && sandi.equals(binding.passuword.text.toString()) && surel != "" && sandi != ""
            ) {
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putInt("Skip", 1)
                editor.apply()
                it.findNavController().navigate(R.id.action_login_Page_to_home1)
                Toast.makeText(
                    activity,
                    "Selamat $namauser anda Berhasil Login",
                    Toast.LENGTH_SHORT

                ).show()
            } else {
                Toast.makeText(activity, "Email dan Password tidak Sesuai", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        binding.tunjukkan.setOnClickListener {
            a++
            var b = a.mod(2)
            if (b == 1) {
                open()
            } else {
                clos()
            }

        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun open() {
        binding.passuword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.tunjukkan.setBackgroundResource(R.drawable.eyes)
    }

    private fun clos() {
        binding.passuword.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.tunjukkan.setBackgroundResource(R.drawable.closed)
    }
    private fun skip(){
        findNavController().navigate(R.id.action_login_Page_to_home1)

    }

}
