package com.example.challenge4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import com.example.challenge4.databinding.FragmentRegisterBinding


class Register : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private val sharedPreferences = "Illyasviel Kawaii"
    var re = Regex("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{8,}")
    var a = 0
    var b = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        binding.savedata.setOnClickListener {
            if (binding.username.text.toString().isNotEmpty() && binding.email.text.toString().isNotEmpty() && binding.pasuworld.text.toString().isNotEmpty()
                && binding.passworld.text.toString().isNotEmpty()
               ) {
                if (!binding.username.text.toString().contains(" ")) {
                if (Patterns.EMAIL_ADDRESS.matcher(binding.email.text).matches()) {
                    if (binding.pasuworld.text.toString().matches(re)) {
                        if (binding.passworld.text.toString() != binding.pasuworld.text.toString()) {
                            Toast.makeText(context, "Password tidak sesuai!", Toast.LENGTH_SHORT)
                                .show();
                        } else {
                            it.findNavController().navigate(R.id.action_register_to_login_Page)
                            Toast.makeText(context, "Registrasi Berhasil", Toast.LENGTH_SHORT)
                                .show()
                        }
                    } else {
                        Toast.makeText(
                            context,
                            "Password terdiri dari 8 huruf, Mengandung 1 Huruf Kapital, Huruf Kecil, Angka, dan Simbol!",
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                } else {
                    Toast.makeText(context, "Format Email Salah!", Toast.LENGTH_SHORT).show()
                }
            }else{
                    Toast.makeText(context, "Username Tidak Boleh Mengandung Spasi!", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(context,"Isi semua data!",Toast.LENGTH_SHORT).show()
            }
            val sharedPreferences: SharedPreferences =
                requireActivity().getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
            val username: String = binding.username.text.toString()
            val email: String = binding.email.text.toString()
            val password: String = binding.passworld.text.toString()

            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("nama", username)
            editor.putString("surel",email)
            editor.putString("sandi",password)
            editor.apply()
        }
        binding.p1.setOnClickListener {
            a++
            var b = a.mod(2)
            if (b == 1) {
                open()
            }
            else {
                clos()
            }
        }
        binding.p2.setOnClickListener {
            b++
            var c = b.mod(2)
            if (c == 1) {
                open1()
            }
            else {
                clos1()
            }
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }
    private fun open(){
        binding.passworld.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.p1.setBackgroundResource(R.drawable.eyes)
    }
    private fun open1(){
        binding.pasuworld.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.p2.setBackgroundResource(R.drawable.eyes)
    }
    private fun clos(){
        binding.passworld.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.p1.setBackgroundResource(R.drawable.closed)
    }
    private fun clos1(){
        binding.pasuworld.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.p2.setBackgroundResource(R.drawable.closed)
    }
}

