package com.example.challenge4

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.challenge4.fragments.list.Home1
import com.example.challenge4.fragments.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_hapus__dialog.*
import kotlinx.android.synthetic.main.fragment_hapus__dialog.view.*
import kotlinx.android.synthetic.main.fragment_home1.*


class Hapus_Dialog : DialogFragment() {
    private lateinit var mUserViewModel: UserViewModel
    private val args by  navArgs<Hapus_DialogArgs>()
    private val sharedPreferences = "Illyasviel Kawaii"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_hapus__dialog, container, false)
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        return view
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        delete.setOnClickListener {
            val sharedPreferences: SharedPreferences =
            requireActivity().getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
            val value2 = -1
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putInt("Value2", value2)
            editor.apply()
            deleteUser()
        }
        cancel.setOnClickListener {
            Toast.makeText(activity,"aksi dibatalkan",Toast.LENGTH_SHORT).show()
            dialog?.dismiss()

        }
    }
    private fun deleteUser() {
            mUserViewModel.deleteUser(args.currentUser)
            Toast.makeText(requireContext(),"Successfully removed!",Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_hapus_Dialog_to_home1)
        }

    }

