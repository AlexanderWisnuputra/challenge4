package com.example.challenge4.fragments.add

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.challenge4.R
import com.example.challenge4.fragments.model.User
import com.example.challenge4.fragments.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_input.*
import kotlinx.android.synthetic.main.fragment_input.view.*

class input : DialogFragment() {
    private lateinit var mUserViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_input, container, false)
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        view.button.setOnClickListener {
            insertDataToDatabase()
        }
        return view
    }

    private fun insertDataToDatabase() {
        val firstName = Judul.text.toString()
        val lastName = Catatan.text.toString()

        if(inputCheck(firstName, lastName)){
            val user = User(0, firstName, lastName)
            mUserViewModel.addUser(user)
            Toast.makeText(requireContext(), "Berhasil Ditambah!", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_input_to_home1)
        }else{Toast.makeText(requireContext(), "Isi semua data!", Toast.LENGTH_LONG).show()}
    }

    private fun inputCheck(firstName: String, lastName: String): Boolean{
        return !(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName))
    }
}