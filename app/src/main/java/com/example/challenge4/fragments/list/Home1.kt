package com.example.challenge4.fragments.list

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challenge4.R
import com.example.challenge4.databinding.FragmentHome1Binding
import com.example.challenge4.fragments.viewmodel.UserViewModel


class Home1 : Fragment() {
    private var _binding: FragmentHome1Binding? = null
    private val binding get() = _binding!!
    private val sharedPreferences = "Illyasviel Kawaii"
    private lateinit var mUserViewModel: UserViewModel
    var a = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHome1Binding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (a == 0) {Toast.makeText(activity, "Tekan Back lagi untuk Keluar", Toast.LENGTH_SHORT).show()}
                a++
                if (a == 2) {System.exit(0)}
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(callback)

        val recyclerView = _binding!!.recyclerview
        val adapter = Homedapter()

        binding.recyclerview.adapter = adapter
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        mUserViewModel.readAllData.observe(viewLifecycleOwner, Observer { user ->
            adapter.setData(user)
            if(adapter.itemCount.equals(0)){
                binding.kosong.setVisibility(View.VISIBLE)
            }
        })

        val sharedPreferences: SharedPreferences =
            requireActivity().getSharedPreferences(sharedPreferences, Context.MODE_PRIVATE)
        val namauser = sharedPreferences.getString("nama", "")

        binding.Welcome.text = "Welcome $namauser!"
        binding.Logout.setOnClickListener {
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putInt("Skip", 0)
            editor.apply()
            it.findNavController().navigate(R.id.action_home1_to_login_Page)
        }
        binding.add.setOnClickListener {
            findNavController().navigate(R.id.action_home1_to_input)
        }
        return (binding.root)
    }
    
}


