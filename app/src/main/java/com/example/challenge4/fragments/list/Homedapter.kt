package com.example.challenge4.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.challenge4.R
import com.example.challenge4.fragments.model.User
import kotlinx.android.synthetic.main.list.view.*

class Homedapter : RecyclerView.Adapter<Homedapter.MyViewHolder>() {
    private var listnote = emptyList<User>()
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list, parent, false))
    }

    override fun getItemCount(): Int {
        return listnote.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = listnote[position]
  
        holder.itemView.ids.text = currentItem.id.toString()
        holder.itemView.titel.text = currentItem.judul
        holder.itemView.note.text = currentItem.catatan
        holder.itemView.edits.setOnClickListener {
            val action = Home1Directions.actionHome1ToEdit(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
        holder.itemView.trash.setOnClickListener {
            val action2 = Home1Directions.actionHome1ToHapusDialog(currentItem)
            holder.itemView.findNavController().navigate(action2)
        }
    }

    fun setData(user: List<User>){
        this.listnote = user
        notifyDataSetChanged()
    }


}
