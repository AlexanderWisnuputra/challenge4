package com.example.challenge4

import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.challenge4.fragments.model.User
import com.example.challenge4.fragments.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.android.synthetic.main.fragment_edit.view.*


class edit : DialogFragment() {
    private val args by  navArgs<editArgs>()
    private lateinit var mUserViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit, container, false)
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        view.valjudul.setText(args.currentuser.judul)
        view.valcatatan.setText(args.currentuser.catatan)
        return view
    }
    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        update.setOnClickListener {
            updateItem()
        }
        }
    private fun updateItem() {
        val firstName = valjudul.text.toString()
        val lastName = valcatatan.text.toString()

        if (inputCheck(firstName, lastName)) {
            // Create User Object
            val updatedUser = User(args.currentuser.id, firstName, lastName)
            // Update Current User
            mUserViewModel.updateUser(updatedUser)
            Toast.makeText(requireContext(), "Updated Successfully!", Toast.LENGTH_SHORT).show()
            // Navigate Back
            findNavController().navigate(R.id.action_edit_to_home1)
            dialog?.dismiss()

        } else {
            Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun inputCheck(firstName: String, lastName: String): Boolean {
        return !(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName))}
}
